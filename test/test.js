const Nightmare = require('nightmare');
const assert = require('assert');

"use strict";

describe('Nightmarejs Tests:', function () {
    this.timeout('30s');

    let nightmare = null;
    beforeEach(() => {
        nightmare = new Nightmare({show: true})
    });

    describe('Sample Test Suite', () => {
        describe('Smoke test', () => {
            it('should open test site', done => {
                nightmare.goto('http://localhost:3000/')
                    .end()
                    .then((result) => {
                        done()
                    })
                    .catch(done)
            })
        });

        describe('Login fail test', () => {
            it('should enter wrong credentials', done => {
                nightmare.goto('https://test.cardpay.com/antifraud/')
                    .end()
                    .on('page', (type, message) => {
                        if (type == 'alert') done()
                    })
                    .type('#username', 's.sales')
                    .type('#password', 'wrong password')
                    .click('#kc-login')
                    .wait(2000)
                    .then((result) => {
                        done()
                    })
                    .catch(done)
            })
        });

        describe('Antifraud smoke test', () => {
            it('should enter login, select UTC and Payments', done => {
                nightmare.goto('https://test.cardpay.com/antifraud/')
                    .type('#username', 's.sales')
                    .type('#password', '123')
                    .click('#kc-login')
                    .wait('#selectTimezone > option')
                    .select('#selectTimezone > option', 'UTC')
                    .select('span.text', 'Payments')
                    .wait('#payments-grid img.load_next')
                    .then((result) => {
                        done()
                    })
                    .catch(done)
            })
        })

    })
});