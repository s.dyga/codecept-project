let express = require('express');
let app = express();
let path = require('path');

app.set('view engine', 'pug');
app.set('views', './views');

app.get('/', function (req, res) {
    res.render('index', {title: 'Test Site', message: 'Home Page!'})});

app.get('/page1', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'))});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});